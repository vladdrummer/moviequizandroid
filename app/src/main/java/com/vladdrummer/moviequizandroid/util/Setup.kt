package com.vladdrummer.moviequizandroid.util

object Setup {
    const val URL = "http://78.24.223.50:8030/moviequiz/"
    const val SPREF = "MovieQuizPref"
    const val MAX_TIME_SEC = 60F
}