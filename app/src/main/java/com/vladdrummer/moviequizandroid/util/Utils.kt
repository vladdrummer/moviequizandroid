package com.vladdrummer.moviequizandroid.util

import android.text.TextUtils
import com.vladdrummer.moviequizandroid.model.QuizQuestion

object Utils {

    fun getPoster(youtube: String) : String{
        return "https://img.youtube.com/vi/$youtube/maxresdefault.jpg"
    }
    fun getLowResPoster(youtube: String) : String{
        return "https://img.youtube.com/vi/$youtube/hqdefault.jpg"
    }
    fun checkQuizQuestionsCorrect(quiz: List<QuizQuestion>): Boolean{
        quiz.forEach {
            if (it == null || TextUtils.isEmpty(it.question) || it.answers == null || it.answers.size != 3) return false
            it.answers.forEach {answer->
                if (TextUtils.isEmpty(answer)) return false
            }
        }
        return true
    }
}