package com.vladdrummer.moviequizandroid.model

import com.vladdrummer.moviequizandroid.R
import kotlin.random.Random

class IntroCellViewPresenter(private val hor: Boolean) {

    var x = 0F
    var y = 0F

    var width = 0F
    var height = 0F

    private val intervals = arrayListOf<Interval>()
    companion object{

        const val MAX_INDEX = 100
        const val FADE_TIME = 600L
        const val FADE_PAUSE = 200L

        val VERTS = arrayOf(R.drawable._01, R.drawable._02, R.drawable._03, R.drawable._04, R.drawable._05, R.drawable._06, R.drawable._07,
            R.drawable._08, R.drawable._09, R.drawable._10, R.drawable._11, R.drawable._12, R.drawable._13, R.drawable._14, R.drawable._15,
            R.drawable._16, R.drawable._17, R.drawable._18, R.drawable._19, R.drawable._20)
        val HORS = arrayOf(R.drawable.hor_01, R.drawable.hor_02, R.drawable.hor_03, R.drawable.hor_04, R.drawable.hor_05, R.drawable.hor_06, R.drawable.hor_07
            , R.drawable.hor_08, R.drawable.hor_09, R.drawable.hor_10, R.drawable.hor_11, R.drawable.hor_12, R.drawable.hor_13, R.drawable.hor_14
            , R.drawable.hor_15, R.drawable.hor_16, R.drawable.hor_17, R.drawable.hor_18, R.drawable.hor_19, R.drawable.hor_20)
    }

    init {
        init()
    }

    private fun init(){
        intervals.clear()
        val startTime = System.currentTimeMillis()
        var cursorTime = startTime
        for (i in 0..MAX_INDEX){
            val start = cursorTime
            val midStop = start + FADE_TIME
            val midStart = midStop + FADE_PAUSE
            val end = midStart + FADE_TIME
            val ingoingRid = if (i == 0) if (hor) HORS[Random.nextInt(0, HORS.size)] else VERTS[Random.nextInt(0, VERTS.size)]
            else intervals[i - 1].outgoingRid
            val outgoingRid = if (hor) HORS[Random.nextInt(0, HORS.size)] else VERTS[Random.nextInt(0, VERTS.size)]
            val afterPause = end + Random.nextLong(1200L, 4000L)
            intervals.add(
                Interval(
                    start,
                    midStop,
                    midStart,
                    end,
                    afterPause,
                    ingoingRid,
                    outgoingRid
                )
            )
            cursorTime = afterPause
        }
    }

    fun getPicData() :IntroCellData{
        val currentTime = System.currentTimeMillis()
        val current = intervals.firstOrNull {interval ->
            interval.start <= currentTime && interval.afterPause >= currentTime
        }
        if (current == null) {
            init()
            return getPicData()
        }
        val picRid = if (currentTime <= current.midStop) current.ingoingRid
            else current.outgoingRid
        var alpha = when (currentTime) {
            in current.start .. current.midStop -> 255 - ((currentTime - current.start).toFloat() / ((current.midStop - current.start).toFloat() / 255F)).toInt()
            in current.midStop..current.midStart -> 0
            in current.midStart..current.end -> ((currentTime - current.midStart).toFloat() / ((current.end - current.midStart).toFloat() / 255F)).toInt()
            else -> 255
        }

        return IntroCellData(
            picRid,
            alpha
        )
    }

    data class IntroCellData(
        val picRid : Int,
        val alpha: Int
    )
    private data class Interval (
        val start: Long,
        val midStop: Long,
        val midStart: Long,
        val end: Long,
        val afterPause: Long,
        val ingoingRid: Int,
        val outgoingRid: Int
    )
}