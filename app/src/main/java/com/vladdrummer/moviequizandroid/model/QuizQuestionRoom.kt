package com.vladdrummer.moviequizandroid.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Room is a mess and TypeConverters works bad.
 */
@Entity
class QuizQuestionRoom {
    @PrimaryKey
    var id: Int = 0
    var movie_id: Int = 0
    var question: String = ""
    var answers: String = ""
    var correctAnswer = ""

    fun fromQuizQuestion(quizQuestion: QuizQuestion) : QuizQuestionRoom {
        val gson = Gson()
        this.id = quizQuestion.id
        this.movie_id = quizQuestion.movie_id
        this.question = quizQuestion.question
        this.answers = gson.toJson(quizQuestion)
        this.correctAnswer = correctAnswer
        return this
    }

    fun toQuizQuestion(): QuizQuestion{
        val gson = Gson()
        val listType = object : TypeToken<ArrayList<String>>() {}.type
        val answers = gson.fromJson(this.answers, listType) as ArrayList<String>
        return QuizQuestion(
            this.id,
            this.movie_id,
            this.question,
            answers
        )
    }
}
