package com.vladdrummer.moviequizandroid.model

data class VersionResponse (
    val version: Int,
    val defaultLocale : String,
    val availableLocales: List<String>,
    val url : String
)