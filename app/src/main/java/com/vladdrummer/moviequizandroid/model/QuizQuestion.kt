package com.vladdrummer.moviequizandroid.model

import androidx.room.PrimaryKey

/**
 * Question for a movie, 3rd question is always right, shuffle before showing
 */
data class QuizQuestion (
    @PrimaryKey
    val id: Int,
    val movie_id: Int,
    val question: String,
    var answers: ArrayList<String>
) {
    var correctAnswer = ""
}
