package com.vladdrummer.moviequizandroid.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 *
 */
@Entity
class BaseRoom {
    @PrimaryKey
    var id = 1
    var version = 0
    var movies: String? = null
    var strings : String? = null

    fun fromBase(base: Base): BaseRoom{
        val gson = Gson()
        this.id = base.id
        this.version = base.version
        this.movies = gson.toJson(base.movies)
        this.strings = gson.toJson(base.strings)
        return  this
    }

    fun toBase() : Base{
        val gson = Gson()
        val mapType = object : TypeToken<HashMap<String, String>>() {}.type
        val strings = gson.fromJson(this.strings, mapType)  as HashMap<String, String>
        val listType = object : TypeToken<ArrayList<Movie>>() {}.type
        val movies = gson.fromJson(this.movies, listType) as ArrayList<Movie>
        return Base(
            this.version,
            movies,
            strings
        )
    }

}