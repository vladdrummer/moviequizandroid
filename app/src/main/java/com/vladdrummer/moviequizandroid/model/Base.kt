package com.vladdrummer.moviequizandroid.model


data class Base (
    val version: Int,
    val movies: List<Movie>,
    val strings : HashMap<String, String>
) {
    val id = 1
}