package com.vladdrummer.moviequizandroid.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Movie (
    val name : String,
    @PrimaryKey
    val id : Int,
    val youtube : String,
    val offset : Int,
    val year : Int
)