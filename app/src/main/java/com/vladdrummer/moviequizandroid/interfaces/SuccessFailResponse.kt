package com.vladdrummer.moviequizandroid.interfaces

interface SuccessFailResponse {
    fun onSuccess(obj: Any)
    fun onFail(reason: String)
}