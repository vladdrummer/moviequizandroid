package com.vladdrummer.moviequizandroid.interfaces

interface SuccessResponse {
    fun onSuccess(obj: Any?)
}