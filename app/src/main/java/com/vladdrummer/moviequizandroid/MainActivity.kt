package com.vladdrummer.moviequizandroid

import android.os.Build
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.google.gson.Gson
import com.vladdrummer.moviequizandroid.data.Repo
import com.vladdrummer.moviequizandroid.model.Movie
import com.vladdrummer.moviequizandroid.ui.game.GameFragmentDirections
import com.vladdrummer.moviequizandroid.ui.intro.IntroFragmentDirections
import com.vladdrummer.moviequizandroid.ui.results.ResultsFragmentDirections
import com.vladdrummer.moviequizandroid.ui.selectcategory.SelectCategoryFragment
import com.vladdrummer.moviequizandroid.ui.selectcategory.SelectCategoryFragmentDirections


class MainActivity : AppCompatActivity() {

    lateinit var navController: NavController
    private val handler = Handler()
    private lateinit var runnable : Runnable
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        hideUI()
        setContentView(R.layout.activity_main)
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        runnable = Runnable {
            hideUI()
            handler.postDelayed(runnable, 5000L)
        }

        handler.post(runnable)
    }

    private fun hideUI(){
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
        if (Build.VERSION.SDK_INT >= VERSION_CODES.KITKAT) {
            val UI_OPTIONS =
                View.SYSTEM_UI_FLAG_IMMERSIVE or
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_FULLSCREEN

            window.decorView.systemUiVisibility = UI_OPTIONS
        }
    }
    override fun onDestroy() {
        handler.removeCallbacks(runnable)
        Repo.onDestroyApp()
        super.onDestroy()
    }

    fun onToSelectionScreen(){
        navController.navigate(IntroFragmentDirections.actionMainFragmentToSelectCategoryFragment())
    }

    fun onToGameScreen(movie: Movie){
        navController.navigate(SelectCategoryFragmentDirections.selectToGameFragment(Gson().toJson(movie)))
    }

    fun onToGameOverScreen(points: Int, outOf: Int, jsonedMovie: String, overall: Int){
        navController.navigate(GameFragmentDirections.actionGameFragmentToResultsFragment(points,outOf, jsonedMovie, overall))
    }

    fun restartEverything(){
        navController.navigate(ResultsFragmentDirections.actionResultsFragmentToSelectCategoryFragment())
    }

    fun goBack(){
        navController.popBackStack()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val navHostFragment: NavHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val current = navHostFragment!!.childFragmentManager.fragments[0]
        if (current is SelectCategoryFragment) {
            finish()
        }
    }

}
