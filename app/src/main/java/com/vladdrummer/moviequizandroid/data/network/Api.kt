package com.vladdrummer.moviequizandroid.data.network

import com.vladdrummer.moviequizandroid.model.Base
import com.vladdrummer.moviequizandroid.model.QuizQuestion
import com.vladdrummer.moviequizandroid.model.VersionResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface Api {
    @GET("version")
    fun checkCurrentVersion(@Query("locale") locale: String, @Query("moviesNumber") moviesNumber: Int?): Single<VersionResponse?>

    @GET("base")
    fun getMoviesData(@Query("locale") locale: String) : Single<Base>

    @GET("quiz")
    fun getQuiz(@Query("locale") locale: String, @Query("movieId") movieId: Int) : Single<List<QuizQuestion>>
}