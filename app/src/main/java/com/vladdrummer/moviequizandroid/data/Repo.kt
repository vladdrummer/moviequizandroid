package com.vladdrummer.moviequizandroid.data

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.vladdrummer.moviequizandroid.data.network.ApiImpl
import com.vladdrummer.moviequizandroid.data.room.DbManager
import com.vladdrummer.moviequizandroid.model.Base
import com.vladdrummer.moviequizandroid.model.VersionResponse
import com.vladdrummer.moviequizandroid.util.Const
import com.vladdrummer.moviequizandroid.util.Setup
import com.vladdrummer.moviequizandroid.interfaces.SuccessResponse
import io.reactivex.disposables.CompositeDisposable
import java.util.*

object Repo {

    val api = ApiImpl
    val dbManager = DbManager
    lateinit var base: Base
    private set
    private var availableLocales: List<String> = mutableListOf()
    private set
    private var defaultLocale : String? = null
    private var sPref : SharedPreferences? = null
    private var initResponse : SuccessResponse? = null
    var compositeDisposable = CompositeDisposable()

    fun getLocale() : String{
        var defaultLocale = "ru"
        this.defaultLocale?.let{
            defaultLocale = it
        }
        val currentLocale = Locale.getDefault().language
        val foundMatch = availableLocales.firstOrNull { it == currentLocale }
        if (foundMatch != null) return foundMatch
        return  defaultLocale
    }

    fun getVersion(): Int{
        sPref?.let {sPref ->
            return sPref.getInt(Const.VERSION_KEY,0)
        }
        return 0
    }

    fun saveVersion(version: Int){
        sPref?.let {sPref ->
            sPref.edit().putInt(Const.VERSION_KEY, version).apply()
        }
    }

    private fun processVersionResponse(versionResponse: VersionResponse){
        this.availableLocales = versionResponse.availableLocales
        this.defaultLocale = versionResponse.defaultLocale
        if (versionResponse.url.isEmpty()) {
            getExistingBase()
        } else {
            getNewBase()
        }
    }

    fun init (context: Context?, successResponse: SuccessResponse){
        context?.let {
            dbManager.init(context)
        }
        sPref = context?.getSharedPreferences(Setup.SPREF, Context.MODE_PRIVATE)
        this.initResponse = successResponse
        dbManager.getLoсalDb(object :  SuccessResponse{
            override fun onSuccess(obj: Any?) {
                var numberOfMovies = 0
                (obj as Base?)?.let { base ->
                    numberOfMovies = base.movies.size
                }
                compositeDisposable.add(
                    api.checkCurrentVersion(getLocale(), numberOfMovies ).subscribe (
                        {
                            it?.let {
                                processVersionResponse(it)
                                return@subscribe
                            }
                        },
                        {
                            getExistingBase()
                        }
                    )
                )
            }

        })

    }

    private fun getExistingBase(){
        dbManager.getLoсalDb(object : SuccessResponse{
            override fun onSuccess(obj: Any?) {
                (obj as Base?)?.let {base->
                    this@Repo.base = base
                    initResponse?.onSuccess(true)
                }
            }
        })

    }
    private fun saveLocalBase(){
        dbManager.saveDB(base)
    }

    private fun getNewBase(){
        compositeDisposable.add(
            api.getMoviesData(getLocale()).subscribe(
                {
                    this.base = it
                    saveLocalBase()
                    saveVersion(it.version)
                    initResponse?.onSuccess(null)
                },
                {
                    getExistingBase()
                }
            )
        )
    }

    fun onDestroyApp(){
        compositeDisposable.dispose()
    }
}