package com.vladdrummer.moviequizandroid.data.network

import com.vladdrummer.moviequizandroid.model.Base
import com.vladdrummer.moviequizandroid.model.QuizQuestion
import com.vladdrummer.moviequizandroid.model.VersionResponse
import com.vladdrummer.moviequizandroid.util.Setup
import io.reactivex.Single

object ApiImpl : Api {

    private val api =
        RetrofitServiceGenerator.createService(
            Api::class.java,
            Setup.URL
        )

    override fun checkCurrentVersion(locale:String, moviesNumber: Int?): Single<VersionResponse?> {
        return api.checkCurrentVersion(locale, moviesNumber)
    }

    override fun getMoviesData(locale: String): Single<Base> {
        return api.getMoviesData(locale)
    }

    override fun getQuiz(locale: String, movieId: Int): Single<List<QuizQuestion>> {
        return api.getQuiz(locale, movieId)
    }
}