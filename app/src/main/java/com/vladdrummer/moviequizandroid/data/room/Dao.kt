package com.vladdrummer.moviequizandroid.data.room

import androidx.room.*
import androidx.room.Dao
import com.vladdrummer.moviequizandroid.model.BaseRoom
import com.vladdrummer.moviequizandroid.model.QuizQuestionRoom
import io.reactivex.Single

@Dao
interface Dao {

    @Query("SELECT * FROM BaseRoom LIMIT 1")
    fun getBase(): Single<BaseRoom>

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    fun setBase(base: BaseRoom)

    @Query ("SELECT * FROM QuizQuestionRoom WHERE movie_id = :id")
    fun getQuizQuestionsByMoveId(id: Int) : List<QuizQuestionRoom>

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    fun saveQuizQuestion(quizQuestionRoom: QuizQuestionRoom)

}