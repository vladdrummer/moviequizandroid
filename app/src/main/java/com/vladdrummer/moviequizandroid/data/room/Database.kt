package com.vladdrummer.moviequizandroid.data.room

import androidx.room.RoomDatabase
import androidx.room.Database
import com.vladdrummer.moviequizandroid.model.BaseRoom
import com.vladdrummer.moviequizandroid.model.QuizQuestionRoom


@Database(entities = [BaseRoom::class, QuizQuestionRoom::class], version = 1)
abstract class Database : RoomDatabase() {
    abstract fun dao(): Dao
}