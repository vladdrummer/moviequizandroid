package com.vladdrummer.moviequizandroid.data.room

import android.content.Context
import androidx.room.Room
import com.vladdrummer.moviequizandroid.data.Repo
import com.vladdrummer.moviequizandroid.interfaces.SuccessResponse
import com.vladdrummer.moviequizandroid.model.Base
import com.vladdrummer.moviequizandroid.model.BaseRoom
import com.vladdrummer.moviequizandroid.model.QuizQuestion
import com.vladdrummer.moviequizandroid.model.QuizQuestionRoom
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.toObservable


object DbManager {

    private lateinit var database: Database
    fun init(context: Context){
        database = Room.databaseBuilder(context, Database::class.java, "database")
            .build()
    }

    fun getLoсalDb(onSuccessListener: SuccessResponse?){
            Repo.compositeDisposable.add(
                database.dao().getBase().subscribe ({ baseRoom ->
                    onSuccessListener?.onSuccess(baseRoom.toBase())
                },
                    {
                        onSuccessListener?.onSuccess(null)
                    })
            )
    }

    fun saveDB(base: Base){
        Repo.compositeDisposable.add(
            Completable.fromAction{database.dao().setBase(BaseRoom().fromBase(base))}.subscribe ()
        )
    }

    fun saveQuizQuestions(quiz: List<QuizQuestion>){
        Repo.compositeDisposable.add(
            quiz.toObservable().subscribeBy (
                onNext = {quizQuestion ->
                  database.dao().saveQuizQuestion(QuizQuestionRoom().fromQuizQuestion(quizQuestion))
                }
            )
        )
    }
}