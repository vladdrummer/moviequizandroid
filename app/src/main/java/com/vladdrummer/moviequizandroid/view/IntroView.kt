package com.vladdrummer.moviequizandroid.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.View
import androidx.core.content.ContextCompat
import com.vladdrummer.moviequizandroid.R
import com.vladdrummer.moviequizandroid.model.IntroCellViewPresenter


class IntroView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object{
        const val SPEED = .2F
    }
    private var cellHeight = 0F
    var inrtoViewPresenters = arrayListOf<IntroCellViewPresenter>()
    var divider: Float = 0F
    var offset = 0F
    var lastTime = System.currentTimeMillis()

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        inrtoViewPresenters.clear()

        val splash_cell_offset = context.resources.getDimension(R.dimen.splash_cell_offset)
        divider = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
            splash_cell_offset,
            context.resources.displayMetrics
        )
        cellHeight = ((h.toFloat() - (divider * 2F)) / 3F)

        val vertBitmap = (ContextCompat.getDrawable(context, R.drawable._01) as BitmapDrawable).bitmap
        val horBitmap = (ContextCompat.getDrawable(context, R.drawable.hor_01) as BitmapDrawable).bitmap
        var ratio = cellHeight / vertBitmap.height.toFloat()
        var vertWidth = vertBitmap.width.toFloat() * ratio
        var horWidth = horBitmap.width.toFloat() * ratio

        var xCursor = 0F
        var yCursor = 0F
        var counter = 0
        for (y in 0..3) {
            for (x in 0..20) {
                val hor = when (counter) {
                    1 -> true
                    4 -> true
                    else -> false
                }
                val introCellViewPresenter = IntroCellViewPresenter(hor)
                introCellViewPresenter.x = xCursor
                introCellViewPresenter.y = yCursor
                introCellViewPresenter.height = cellHeight
                introCellViewPresenter.width = if (hor) horWidth else vertWidth
                inrtoViewPresenters.add(introCellViewPresenter)
                xCursor += if (hor) horWidth else vertWidth
                xCursor += divider
                counter ++
                if (counter >= 5) {
                    counter = 0
                }
            }
            xCursor = 0F
            yCursor += cellHeight
            yCursor += divider
        }
    }
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        var deltaTime = System.currentTimeMillis() - lastTime
        offset -= deltaTime.toFloat() * SPEED
        if (offset < -2000F) {
            offset = 0F
        }
        inrtoViewPresenters.forEach{inrtoViewPresenter ->
            val data = inrtoViewPresenter.getPicData()
            val bitmap = (ContextCompat.getDrawable(context, data.picRid) as BitmapDrawable).bitmap
            var src = Rect(0,0,bitmap.width, bitmap.height)
            var dest = Rect((inrtoViewPresenter.x + offset).toInt(), inrtoViewPresenter.y.toInt(), ((inrtoViewPresenter.x + offset) + inrtoViewPresenter.width).toInt(), (inrtoViewPresenter.y + inrtoViewPresenter.height).toInt())
            val paint = Paint()
            paint.alpha = data.alpha
            canvas?.drawBitmap(bitmap, src, dest , paint)
        }
        lastTime = System.currentTimeMillis()
        invalidate()
    }
}