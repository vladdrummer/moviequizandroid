package com.vladdrummer.moviequizandroid.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.vladdrummer.moviequizandroid.R

class TimerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
    ) : View(context, attrs, defStyleAttr){

    private var greenPaint= Paint()
    private var greenLightPaint= Paint()
    private var yellowPaint =  Paint()
    private var yellowLightPaint =  Paint()
    private var redPaint = Paint()
    private var redLightPaint = Paint()

    init {
        greenPaint.color = ContextCompat.getColor(context, R.color.time_green)
        greenLightPaint.color = ContextCompat.getColor(context, R.color.time_green_light)
        yellowLightPaint.color =  ContextCompat.getColor(context, R.color.time_yellow_light)
        yellowPaint.color =  ContextCompat.getColor(context, R.color.time_yellow)
        redLightPaint.color = ContextCompat.getColor(context, R.color.time_red_light)
        redPaint.color = ContextCompat.getColor(context, R.color.red)
    }

    var maxTime = 60F
    set(value) {
        field = value
        invalidate()
    }
    var time = 60F
        set(value) {
            field = value
            invalidate()
        }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val share = time / maxTime
        var paint = when (share * 100) {
            in 84F..Float.MAX_VALUE -> greenPaint
            in 68F..84F -> greenLightPaint
            in 51F..68F -> yellowLightPaint
            in 35F..51F -> yellowPaint
            in 18F..35F -> redLightPaint
            else -> redPaint
        }
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 3F

        canvas?.drawLine(10F, canvas.height - 10F, (canvas.width - 20F) * share, canvas.height - 10F, paint)
    }
}

