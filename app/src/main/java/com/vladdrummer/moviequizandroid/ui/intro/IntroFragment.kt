package com.vladdrummer.moviequizandroid.ui.intro

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.vladdrummer.moviequizandroid.MainActivity
import com.vladdrummer.moviequizandroid.R
import kotlinx.android.synthetic.main.fragment_intro.*

class IntroFragment : Fragment() {

    private lateinit var viewModel: IntroViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_intro, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(IntroViewModel::class.java)
        viewModel.ready.observe(this, Observer{ready ->
            if (ready) {
                Handler().postDelayed({(activity as MainActivity).onToSelectionScreen()},7000L)
            }
        })
        viewModel.init(this.context)

    }
}
