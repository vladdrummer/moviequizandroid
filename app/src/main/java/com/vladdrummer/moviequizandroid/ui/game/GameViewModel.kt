package com.vladdrummer.moviequizandroid.ui.game

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vladdrummer.moviequizandroid.data.Repo
import com.vladdrummer.moviequizandroid.model.QuizQuestion
import com.vladdrummer.moviequizandroid.util.Setup
import com.vladdrummer.moviequizandroid.util.Utils
import java.util.*

class GameViewModel : ViewModel(){

    var quiz = MutableLiveData<List<QuizQuestion>>()
    set(value) {
        Collections.shuffle(value.value)
        field = value
    }
    val currentQuizQuestion = MutableLiveData<QuizQuestion>()
    var timer = MutableLiveData<Float>()
    private val handler = Handler()
    var index = -1
    private set
    var points = MutableLiveData<Int>()
    var gameOver = MutableLiveData<Boolean>()
    var gameName = MutableLiveData<String>()

    companion object{
        const val STEP = .05F
        const val DELAY_BETWEEN_QUESTIONS = 1000L
    }

    init {
        gameOver.postValue(false)
    }

    fun getQuiz(movieId: Int) {
        quiz.postValue(listOf())
        Repo.base.movies.firstOrNull { it.id == movieId }?.let {movie->
            gameName.postValue(movie.name)
        }
        Repo.compositeDisposable.add(Repo.api.getQuiz(Repo.getLocale(), movieId).subscribe(
            {
                if (Utils.checkQuizQuestionsCorrect(it)) {
                    saveQuizToDb(it)
                    Collections.shuffle(it)
                    quiz.postValue(it)
                } else {
                    tryObtainingQuizFromLocal()
                }
            },
            {
                tryObtainingQuizFromLocal()
            }))
    }

    private fun saveQuizToDb(quiz: List<QuizQuestion>){
        Repo.dbManager.saveQuizQuestions(quiz)
    }

    private fun tryObtainingQuizFromLocal(){

    }

    fun startGame(){
        points.postValue(0)
        timer.postValue(Setup.MAX_TIME_SEC)
        val lStep = (STEP * 1000).toLong()
        handler.postDelayed(object : Runnable {
            override fun run() {
                timer.value?.let {
                    timer.postValue(it - STEP)
                    if (timer.value!! <= 0){
                        gameOver.postValue(true)
                        handler.removeCallbacks(this)
                    }
                }
                handler.postDelayed(this, lStep)
            }
        }, lStep)
        nextQuestion()
    }

    private fun nextQuestion(){
        quiz.value?.let {
            if (index < it.size - 1) {
                index++
                val quizQuestion = quiz.value?.get(index)
                quizQuestion?.let {
                    it.correctAnswer = it.answers[it.answers.size - 1]
                    Collections.shuffle(it.answers)
                    currentQuizQuestion.postValue(it)
                }
            }else {
                gameOver.postValue(true)
            }
        }
    }

    fun answered(quizQuestion: QuizQuestion, index: Int){
        var correctIndex = 0
        for (i in 0..quizQuestion.answers.size){
            if (quizQuestion.answers[i] == quizQuestion.correctAnswer) {
                correctIndex = i
                break
            }
        }
        if (correctIndex == index) {
            points.value?.let {
                points.postValue(it + 1)
            }
        }
        Handler().postDelayed({ nextQuestion()}, DELAY_BETWEEN_QUESTIONS)
    }
}