package com.vladdrummer.moviequizandroid.ui.results

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.vladdrummer.moviequizandroid.MainActivity
import com.vladdrummer.moviequizandroid.R
import com.vladdrummer.moviequizandroid.data.Repo
import com.vladdrummer.moviequizandroid.model.Movie
import com.vladdrummer.moviequizandroid.util.Utils
import kotlinx.android.synthetic.main.fragment_results.*
import kotlinx.android.synthetic.main.row_select.view.*

class ResultsFragment : Fragment() {

    private val args: ResultsFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_results, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val movie = Gson().fromJson(args.jsonedMovie, Movie::class.java)
        context?.let { context ->
            Glide.with(context).load(Utils.getLowResPoster(movie.youtube)).into(ivPoster)
        }
        var res = 0
        if (args.score == args.overall) {
            Repo.base.strings["bravo_title"]?.let { str ->
                tvResultHeader.text = str
            }
            Repo.base.strings["bravo_text"]?.let { str ->
                tvResults.text = str
            }
            res = R.drawable.ic_game_badge_gold
        } else if (args.score == 0) {
            Repo.base.strings["dont_give_up_title"]?.let { str ->
                tvResultHeader.text = str
            }
            Repo.base.strings["dont_give_up_text"]?.let { str ->
                tvResults.text = str
            }
            res = R.drawable.ic_game_badge_none
        } else {
            Repo.base.strings["total_score_of"]?.let {str->
                tvResults.text = String.format(str , args.score, args.total)
            }
            when (((args.score.toFloat() / args.total.toFloat()) * 100f ).toInt()){
                in 90..Int.MAX_VALUE -> {
                    res = R.drawable.ic_game_badge_gold
                    Repo.base.strings["bravo_title"]?.let { str ->
                        tvResultHeader.text = str
                    }
                }
                in 60..90 ->{
                    res = R.drawable.ic_game_badge_silver
                    Repo.base.strings["awesome_title"]?.let { str ->
                        tvResultHeader.text = str
                    }
                }
                in 0..60 ->{
                    res = R.drawable.ic_game_badge_bronze
                    Repo.base.strings["carry_on_title"]?.let { str ->
                        tvResultHeader.text = str
                    }
                }
            }
        }
        context?.let { context ->
            ivBadge.setImageDrawable(ContextCompat.getDrawable(context, res))
        }
        Repo.base.strings["main_menu"]?.let { str ->
            tvGoBack.text = str
        }
        tvScore.text = "${args.score}"

        llRestart.setOnClickListener {
            (activity as MainActivity).restartEverything()
        }
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            (activity as MainActivity).restartEverything()
        }
    }
}