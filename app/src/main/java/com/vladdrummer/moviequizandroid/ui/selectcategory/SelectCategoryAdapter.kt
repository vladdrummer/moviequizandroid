package com.vladdrummer.moviequizandroid.ui.selectcategory

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vladdrummer.moviequizandroid.R
import com.vladdrummer.moviequizandroid.interfaces.SuccessResponse
import com.vladdrummer.moviequizandroid.model.Movie
import com.vladdrummer.moviequizandroid.util.Utils
import kotlinx.android.synthetic.main.row_select.view.*

    class SelectCategoryAdapter(private val clickListener: SuccessResponse, private val typeface: Typeface): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private var listOfMovies = listOf<Movie>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val vh = MovieListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_select, parent, false))
        vh.itemView.tvName.typeface = typeface
        return vh
    }

    override fun getItemCount(): Int = listOfMovies.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val movieViewHolder = viewHolder as MovieListViewHolder
        movieViewHolder.bindView(listOfMovies[position])
        movieViewHolder.itemView.setOnClickListener { clickListener.onSuccess(listOfMovies[position]) }
    }

    fun setMovieList(listOfMovies: List<Movie>) {
        this.listOfMovies = listOfMovies
        notifyDataSetChanged()
    }

    class MovieListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(movie: Movie) {
            itemView.tvName.text = movie.name
                itemView.ivPoster.setImageDrawable(ContextCompat.getDrawable(itemView.ivPoster.context, R.drawable.hor_01))
                Glide.with(itemView.context).load(Utils.getLowResPoster(movie.youtube)).into(itemView.ivPoster)
        }
    }
}