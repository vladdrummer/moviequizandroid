package com.vladdrummer.moviequizandroid.ui.selectcategory

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vladdrummer.moviequizandroid.data.Repo
import com.vladdrummer.moviequizandroid.model.Movie

class SelectCategoryViewModel : ViewModel() {
    var movies = MutableLiveData<List<Movie>>()

    init {
        movies.postValue(Repo.base.movies)
    }

    fun applyFilter(type: Int){
        //TODO: Filter list
    }

    fun search(keyword: String){
        //TODO: Filter list
    }
}