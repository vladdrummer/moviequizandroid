package com.vladdrummer.moviequizandroid.ui.selectcategory

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.vladdrummer.moviequizandroid.MainActivity
import com.vladdrummer.moviequizandroid.R
import com.vladdrummer.moviequizandroid.data.Repo
import com.vladdrummer.moviequizandroid.interfaces.SuccessResponse
import com.vladdrummer.moviequizandroid.model.Movie
import kotlinx.android.synthetic.main.fragment_select_category.*

class SelectCategoryFragment : Fragment(){

    private lateinit var viewModel: SelectCategoryViewModel
    private lateinit var adapter : SelectCategoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_select_category, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val bold = Typeface.createFromAsset(context?.assets, "fonts/Inter-Bold.otf")
        adapter = SelectCategoryAdapter(object :SuccessResponse{
            override fun onSuccess(obj: Any?) {
                (activity as MainActivity).onToGameScreen(obj as Movie)
            }
        }, bold)
        viewModel = ViewModelProviders.of(this).get(SelectCategoryViewModel::class.java)
        val rowNumber = resources.getInteger(R.integer.number_of_movies_per_row)
        rv.layoutManager = GridLayoutManager(context, rowNumber)
        rv.adapter = adapter
        viewModel.movies.observe(this, Observer {
            adapter.setMovieList(it)
        })

    }
}