package com.vladdrummer.moviequizandroid.ui.game

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import com.google.gson.Gson
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.vladdrummer.moviequizandroid.MainActivity
import com.vladdrummer.moviequizandroid.R
import com.vladdrummer.moviequizandroid.data.Repo
import com.vladdrummer.moviequizandroid.databinding.FragmentGameBinding
import com.vladdrummer.moviequizandroid.model.Movie
import com.vladdrummer.moviequizandroid.util.Setup
import kotlinx.android.synthetic.main.fragment_game.*

class GameFragment : Fragment(){

    private lateinit var viewModel: GameViewModel
    private val args: GameFragmentArgs by navArgs()
    lateinit var movie: Movie
    var tvAnswers = listOf<TextView>()
    var tvAnswerFls = listOf<FrameLayout>()
    lateinit var binding:FragmentGameBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
        DataBindingUtil.inflate(inflater,R.layout.fragment_game, container,false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        timerView.maxTime = Setup.MAX_TIME_SEC
        tvAnswers = listOf(tvAnswer1, tvAnswer2, tvAnswer3)
        tvAnswerFls = listOf(flAnswer1, flAnswer2, flAnswer3)
        viewModel = ViewModelProviders.of(this).get(GameViewModel::class.java)
        initObservers()
        movie = Gson().fromJson(args.jsonedMoview, Movie::class.java)
        viewModel.getQuiz(movie.id)
        ivBack.setOnClickListener {
            (activity as MainActivity).goBack()
        }
        initYoutubePlayer()
    }

    private fun initYoutubePlayer() {
        youtube_player_view.getPlayerUiController().showUi(false)
        youtube_player_view.getPlayerUiController().showVideoTitle(false)
        youtube_player_view.getPlayerUiController().showCustomAction1(false)
        youtube_player_view.getPlayerUiController().showCustomAction2(false)
        youtube_player_view.enableBackgroundPlayback(true)
        //    youtube_player_view.inflateCustomPlayerUi()
        lifecycle.addObserver(youtube_player_view)
    }

    private fun initObservers() {
        viewModel.quiz.observe(this, Observer {
            val hasData = it.isNotEmpty()
            showGameView(hasData)
            if (hasData) {
                startGame()
            }
        })
        viewModel.gameName.observe(this, Observer {
            tvName.text = it
        })
        viewModel.timer.observe(this, Observer {
            timerView.time = it
        })
        viewModel.points.observe(this, Observer {
            tvScore.text = "$it"

        })
        binding.score = Repo.base.strings["score"] + " : %s"
        viewModel.currentQuizQuestion.observe(this, Observer { quizQuestion ->
            binding.currentQuizQuestion = quizQuestion
            context?.let {context->
//                tvAnswers.forEach {
//                    it.setBackgroundColor(
//                        ContextCompat.getColor(
//                            context,
//                            android.R.color.transparent
//                        )
//                    )
//                }
                tvAnswerFls.forEach {
                    it.background = ContextCompat.getDrawable(context, R.drawable.tv_bg)
                }
            }
            val answerListener = View.OnClickListener { view ->
                val index = when (view.id) {
                    R.id.flAnswer1 -> 0
                    R.id.flAnswer2 -> 1
                    else -> 2
                }
                context?.let {
                    tvAnswerFls[index].background =
                        ContextCompat.getDrawable(
                            it, if (quizQuestion.answers[index] == quizQuestion.correctAnswer)
                                R.drawable.tv_bg_green else R.drawable.tv_bg_red)
                }
                viewModel.answered(quizQuestion, index)
            }
            flAnswer1.setOnClickListener(answerListener)
            flAnswer2.setOnClickListener(answerListener)
            flAnswer3.setOnClickListener(answerListener)
        })
        viewModel.gameOver.observe(this, Observer {
            viewModel.points.value?.let {
                viewModel.quiz.value?.let {quiz->
                    (activity as MainActivity).onToGameOverScreen(it, viewModel.index, Gson().toJson(movie), quiz.size)
                }
            }
        })
    }

    private fun startGame(){
        youtube_player_view.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                youTubePlayer.setVolume(0)
                youTubePlayer.loadVideo(movie.youtube, movie.offset.toFloat())
                viewModel.startGame()
                Handler().postDelayed({
                    context?.let { context ->
                        val anim = AnimationUtils.loadAnimation(context, R.anim.up)
                        anim.setAnimationListener(object : Animation.AnimationListener{
                            override fun onAnimationRepeat(animation: Animation?) {}

                            override fun onAnimationEnd(animation: Animation?) {
                                context?.let {
                                rlToolBar.visibility = View.GONE
                                }
                            }

                            override fun onAnimationStart(animation: Animation?) {
                            }

                        })
                        rlToolBar.startAnimation(anim)
                    }
                },7000L)
            }
        })
    }

    private fun showGameView(show: Boolean){
        rlGameView.visibility = if (show) View.VISIBLE else View.GONE
        pb.visibility = if (show) View.GONE else View.VISIBLE
    }
}