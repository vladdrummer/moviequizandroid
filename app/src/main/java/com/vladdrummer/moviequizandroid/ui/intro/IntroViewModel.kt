package com.vladdrummer.moviequizandroid.ui.intro

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vladdrummer.moviequizandroid.data.Repo
import com.vladdrummer.moviequizandroid.interfaces.SuccessResponse

class IntroViewModel : ViewModel() {

    var ready = MutableLiveData<Boolean>()
    var btnGoName = MutableLiveData<String>()

    fun init(context: Context?) {
        ready.postValue(false)
        Repo.init(context, object :
            SuccessResponse {
            override fun onSuccess(obj: Any?) {
                ready.postValue(true)
                btnGoName.postValue(Repo.base.strings["start_game"])
            }
        })
    }
}
